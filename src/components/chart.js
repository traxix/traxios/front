import React from 'react';
import Chart from "chart.js";

import config from "../../site-config.js"

export default class LineGraph extends React.Component {
    chartRef = React.createRef();
    constructor(props) {
	super(props);
	console.log(JSON.stringify(props));
        
    }

    
    componentDidMount() {
        const myChartRef = this.chartRef.current.getContext("2d");
	const args = this.props.url.pathname.split('/');
	let avg_period = 10000;
	if (args[3] === undefined) {
            console.log("default time to " + avg_period);
	} else {
	    avg_period = args[3];
	}
        const url = config.metric_url + '/' + args[2] + '/' + avg_period;
        console.log('url> ' + url);
	fetch(url)
            .then(res => res.json())
            .then(json => {
                const foo = {
		    type: "line",
		    data: json,
                    options: {
                        elements: {
                            line: {
                                tension: 0 // disables bezier curves
                            }
                        },
                        scales: {
			    xAxes: [{
                                type: 'linear',
                                position: 'bottom'
			    }]
                        }
		    }
                };

	        console.log(foo);
                new Chart(myChartRef, {
		    type: "line",
		    data: json,
		    options: {
                        scales: {
			    xAxes: [{
                                type: 'linear',
                                position: 'bottom'
			    }]
                        }
		    }
                }
                         );
	        this.setState({id: args[2]});
            });
        
    }

    render() {
        return (
		<div>
		<canvas
	    width="400" height="400"
            id="myChart"
            ref={this.chartRef}
		/>
		</div>
        );
    }
}



// new Chart(myChartRef, {
// 	type: "line",
// 	data: {
//         datasets:[
// 		{data: [
//                 {x:0, y:0},
//                 {x:1, y:1},
//                 {x:2, y:2},
//                 {x:3, y:3},
//                 {x:4, y:4},
//                 {x:5, y:5},
//                 {x:6, y:6},
// 		]},
// 		{data: [
//                 {x:0, y:0},
//                 {x:1, y:1},
//                 {x:2, y:1},
//                 {x:3, y:2},
//                 {x:4, y:3},
//                 {x:5, y:5},
//                 {x:6, y:8},
// 		]},
//         ]
// 	},
// 	options: {
//         scales: {
// 		xAxes: [{
//                 type: 'linear',
//                 position: 'bottom'
// 		}]
//         }
// 	}
