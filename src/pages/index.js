import React from "react";
import { Link } from "gatsby";
import 'bootstrap/dist/css/bootstrap.css';
import Card from "react-bootstrap/Card";
import SyntaxHighlighter from 'react-syntax-highlighter';
import { dark } from 'react-syntax-highlighter/dist/esm/styles/hljs';

import config from "../../site-config.js"

const IndexPage = (props) => (
    <div>
      <div class="jumbotron jumbotron-fuid bg-dark text-white" align="center">
        <h1 class="display-1 font-weight-bold" style={{ color: 'CornflowerBlue' }} >EZmon</h1>
        <p class="display-4">Monitore your machine in less than a minute </p>
        <SyntaxHighlighter key="install_line" language="shell" style={dark}>
	{config.install_command}
        </SyntaxHighlighter>
      </div>

      <div align="center">
        <div class="w-75 "  >
          <div class="d-flex flex-row  justify-content-between ">
            <Card style={{ width: '18rem' }}>
              <Card.Body>
                <Card.Title>Free</Card.Title>
                {/* <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle>  */}
                <Card.Text>
                  So you can start as quickly as possible, we will talk about money later.
                </Card.Text>
                {/* <Card.Link href="#">Card Link</Card.Link> */}
                {/* <Card.Link href="#">Another Link</Card.Link> */}
              </Card.Body>
            </Card>      
            <Card style={{ width: '18rem' }}>
              <Card.Body>
                <Card.Title>No account needed</Card.Title>
                {/* <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle>  */}
                <Card.Text>
                  No registration needed to begin. It just work out over the box.
                </Card.Text>
                {/* <Card.Link href="#">Card Link</Card.Link> */}
                {/* <Card.Link href="#">Another Link</Card.Link> */}
              </Card.Body>
            </Card>
            <Card style={{ width: '18rem' }}>
              <Card.Body>
                <Card.Title>Open Source</Card.Title>
                {/* <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle>  */}
                <Card.Text>
                  Total transparency, you can install it on premesis, you can contribute.
                  <br />
                  Know what you eat, know what you use. 
                </Card.Text>
                {/* <Card.Link href="#">Card Link</Card.Link> */}
                {/* <Card.Link href="#">Another Link</Card.Link> */}
              </Card.Body>
            </Card>      
          </div>        
        </div>
      </div>
    </div>
);

export default IndexPage
