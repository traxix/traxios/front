import React, { Component } from 'react';
import Layout from "../components/layout";
import LineGraph from "../components/chart";



const Metrics = (props) => (
    <Layout>
      <LineGraph url={props.location} />
    </Layout>
);


export default Metrics;
